# Prog day service
Service for getting the 256th day of input year

Input date format:

Y - year number, 4 digits (Examples: 1999, 2003)

Example: http://localhost/?year=2019

        {"errorCode": 200, "dataMessage": "13/9/2019"}


Run:

* git clone https://gitlab.com/Shurikk/prog_day_service.git

* gradle startService
