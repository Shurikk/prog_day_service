from flask import Flask, request
from calendar import monthrange
from json import dumps

""" 
Service for getting the 256th day of input year

Input date format:
Y - year number, 4 digits (Examples: 1999, 2003)

Example: http://127.0.0.1:80/?year=2019
        {"errorCode": 200, "dataMessage": "13/9/2019"}
"""

HOST = '0.0.0.0'
PORT = 80

YEAR_ARG = 'year'
YEAR_ARG_LEN = 4
SUCCESS_CODE = 200
ERROR_CODE = 400
ERROR_MSG = 'Invalid year argument'

PROG_DAY = 256
MONTHS_COUNT = 12

app = Flask(__name__)


def get_day_data(year: int) -> (int, int):
    """ For input year returns it's 256 day: month number and day number """
    day_count = PROG_DAY
    month_num = 1
    while month_num <= MONTHS_COUNT:
        month_days_num = monthrange(year, month_num)[1]
        day_count -= month_days_num
        if day_count < 0:
            day_count += month_days_num
            break
        else:
            month_num += 1
    return month_num, day_count


def date_format(day, month, year):
    """ Returns str of date in format dd/mm/yy """
    return f'{str(day).zfill(2)}/{str(month).zfill(2)}/{str(year).zfill(2)}'


def build_output(code, data):
    """ Create json http response
        Example:    (JSON):
                    {
                    "errorCode": 200,
                    "dataMessage": "13/09/17"
                    }
    """
    output_dict = {'errorCode': code,
                   'dataMessage': f'{data}'}
    return dumps(output_dict)


@app.route('/', methods=['GET'])
def get_day():
    """ Flask method to handle GET requests
        args: year
    """
    year = request.args.get(YEAR_ARG, type=str)
    if year is None or len(year) != YEAR_ARG_LEN:
        code = ERROR_CODE
        data = ERROR_MSG
    else:
        code = SUCCESS_CODE
        p_year = int(year)
        p_month, p_day = get_day_data(p_year)
        data = date_format(p_day, p_month, p_year)
    output_data = build_output(code, data)
    return output_data


if __name__ == '__main__':
    app.run(host=HOST, port=PORT)
